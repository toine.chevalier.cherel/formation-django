
# Queryset exemples

### Articles dont le prix est supérieur à 100€
```python
Product.objects.filter(price__gt=100)
```

### Commandes contenant le produit X
```python
Order.objects.filter(items__product__title="Banane")
```

### Produits à aller voler chez les Michel
```python
Product.objects.filter(items__order__user__username="admin")
```

### Commande concernant des vêtements
```python
Order.objects.filter(items__product__categories__title__icontains="Fruit")
```

### Commandes passées en décembre, avant Noel
```python
 Order.objects.filter(created_at__lt=datetime(2020,12,24))
 ```
 Order.objects.filter(created_at__month=12, created_at__day__lt=25)

### Commande dont un des produit est en rupture de stock
```python
Order.objects.filter(items__product__in_stock=True)
```

### Articles dont le prix est supérieur à la moyenne des prix
```python
from django.db.models import Avg
avg = Product.objects.aggregate(Avg('price'))['price__avg']
Product.objecs.filter(price__gt=avg)
```
