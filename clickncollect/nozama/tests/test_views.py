from nozama.models import *
from nozama.views import ProductView
from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import resolve

class TestProductView(TestCase):
    def test_resolve_products_url(self):
        found = resolve("/api/v1/products/")
        self.assertIs(found.func.view_class, ProductView)

#     def test_get_menu_as_json(self):
#         Product.objects.create(title="cafe", price=1)
#         Product.objects.create(title="the", price=9)
#         Product.objects.create(title="chocolat", price=2)

#         reponse = self.client('/api/v1/products/')
        

