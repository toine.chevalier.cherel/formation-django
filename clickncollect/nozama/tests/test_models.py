from nozama.models import Order, Product
from django.test import TestCase
from django.contrib.auth.models import User


class TestOrder(TestCase):
    def test_number_increment(self):
        u = User.objects.create(username='jeanmi')
        o1 = Order(user=u)
        o1.save()
        self.assertEqual(o1.number, 1)
        o2 = Order(user=u)
        o2.save()
        self.assertEqual(o2.number, 2)


    # def test_average(self):
    #     u = User.objects.create(username="toto")
    #     Order.objects.create(number=1, user=u)
    #     Order.objects.create(number=1, user=u)
    #     self.assertEqual(Order.get_max_number(), 3)



# class TestProduct(TestCase):
#     def test_number_increment(self):
#         u = User.objects.create(username='jeanmi')
#         Product.objects.create(title="prod_test1", price=3)
#         Product.objects.create(title="prod_test1", price=3)