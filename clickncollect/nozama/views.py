from django.shortcuts import render
from nozama.models import *
from django.http import HttpResponse, JsonResponse
from django.views.generic import View, DetailView
from rest_framework import viewsets
from nozama.serializers import *

def homepage(request):
    return HttpResponse("Welcome !!")


class ApiView(View):
    def get(self, request, *arg, **kwargs):
        return HttpResponse("Welcome !!") 

class ProductView(View):
    def get(self, request):
        return HttpResponse("\n".join(
            product.title for product in Product.objects.all()
        ))

class ProductDetailView(DetailView):

    model = Product

    def render_to_response(self, context, **kwargs):
        return JsonResponse(context)

    def get_context_data(self, **kwargs):
        return {
            "title": self.object.title,
            "price": self.object.price
        }


class ProductViewSet(viewsets.ViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class OrderViewSet(viewsets.ViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer