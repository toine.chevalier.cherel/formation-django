from django.db import models
from datetime import datetime
import uuid
from django.contrib.auth.models import User


class Category(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "Categories"
    def __str__(self):
        return self.title

class Product(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now=True, auto_now_add=False)
    updated_at = models.DateTimeField(auto_now=True, auto_now_add=False)
    in_stock = models.BooleanField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    photo = models.ImageField(null=True)
    categories = models.ManyToManyField(Category, related_name='products') 

    def __str__(self):
        return self.title


class Order(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    number = models.IntegerField(default=0, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="orders")
    created_at = models.DateTimeField(auto_now=True, auto_now_add=False)
    updated_at = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return f"Order #{self.number:0>6}"
    
    @classmethod
    def get_max_number(cls):
        return Order.objects.all().aggregate(
                models.Max('number')
            )['number__max']

    def save(self, *args, **kwargs) -> None:
        if not self.number:
            max_number = self.get_max_number()
            max_number = max_number + 1 if max_number is not None else 1
            self.number = max_number
        return super().save(*args, **kwargs)


class OrderItem(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="items")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="items")
    number = models.IntegerField()

    def __str__(self):
        return f"{self.uuid}"

