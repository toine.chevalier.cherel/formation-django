from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register("products", views.ProductViewSet)
router.register("orders", views.OrderViewSet)
breakpoint()
router.urls
urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("api/v1/", include(router.urls)),
    # path("api/v1/", views.ApiView.as_view()),
    # path("api/v1/products/", views.ProductView.as_view()),
    # path("api/v1/products/<uuid:pk>/", views.ProductDetailView.as_view())
]
