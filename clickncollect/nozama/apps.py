from django.apps import AppConfig


class NozamaConfig(AppConfig):
    name = "nozama"
    verbose_name = "Nozama.com"
