from django.contrib import admin
from .models import Product, Category, Order, OrderItem
from django.utils.safestring import mark_safe


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "title",
    )

class OrderItemInline(admin.TabularInline):
    model = OrderItem

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'user', 'created_at']
    inlines = [OrderItemInline]



@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = (
        "order",
        "product",
        "number",
    )

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "display_photo",
        "title",
        "description",
        "created_at",
        "updated_at",
        "in_stock",
        "price",
        "photo",
    )

    def display_photo(self, obj):
        return mark_safe(f'<img src="{obj.photo.url}" height=30>')
    display_photo.short_descriptions='heyyyy'

    search_fields = ["uuid", "title", "description"]

